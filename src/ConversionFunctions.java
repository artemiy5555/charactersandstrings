public class ConversionFunctions {

    public static void main(String[] args) {
        System.out.println(numbersToString(10));
        System.out.println(numbersToString(10.10));
        System.out.println(stringToNumber("10"));
        System.out.println(stringToDouble("10.10"));
    }

    public static String numbersToString(int number){
        return String.valueOf(number);
    }

    public static String numbersToString(double number){
        return String.valueOf(number);
    }

    public static int stringToNumber(String str){
        int number =0;
        try {
            number = Integer.parseInt(str);
        }catch (Exception e){
            e.printStackTrace();
        }

        return number;
    }

    public static double stringToDouble(String str){
        double number =0;
        try {
            number = Double.parseDouble(str);
        }catch (Exception e){
            e.printStackTrace();
        }
        return number;
    }

}
