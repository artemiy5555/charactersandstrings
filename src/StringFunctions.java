import java.util.Arrays;

public class StringFunctions {

    public static void main(String[] args) {
        //- Дана строка, состоящая из слов, разделенных пробелами и знаками препинания. Определить длину самого короткого слова.
        System.out.println(shortestWordLength("Просто пример предложения"));
        //- Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"
        System.out.println(editString("Просто пример предложения"));
        //- Добавить в строку пробелы после знаков препинания, если они там отсутствуют.
        System.out.println(addSpaces("Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан."));
        //- Оставить в строке только один экземпляр каждого встречающегося символа.
        System.out.println(OneEks("Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан."));
        //- Подсчитать количество слов во введенной пользователем строке
        System.out.println(countWords("Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан."));
        //- Удалить из строки ее часть с заданной позиции и заданной длины.
        System.out.println(deleteLine("Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан.","Привет"));
        //- Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
        System.out.println(reverseString("Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан."));
        //- В заданной строке удалить последнее слово
        System.out.println(deleteLastString("Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан."));
    }

    public static int shortestWordLength(String str){
        String[] st = str.split(" ");
        int min = st[0].length();
        for(int i = 1; i < st.length; i++){
            min = st[i].length() < min ? st[i].length() : min;
        }
        return min;
    }

    public static StringBuilder addSpaces(String src){

        String delimiters = ".,?!:;";
        StringBuilder result = new StringBuilder();
        boolean prevCharIsDelimiter = false;

        for (int i = 0; i < src.length(); i++) {

            char ch = src.charAt(i);

            if (delimiters.indexOf(ch) != -1) {
                prevCharIsDelimiter = true;

            } else {
                if (ch != ' ' && prevCharIsDelimiter) {
                    result.append(' ');
                }
                prevCharIsDelimiter = false;
            }

            result.append(ch);
        }
        return result;
    }

    public static StringBuilder OneEks(String str){
        char[]chars = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        boolean repeatedChar;
        for (int i = 0; i < chars.length; i++) {
            repeatedChar = false;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i]== chars[j]) {
                    repeatedChar = true;
                    break;
                }
            }
            if (!repeatedChar) {
                sb.append(chars[i]);
            }
        }
        return sb;
    }

    public static int countWords(String str){

        int count = 0;

        if(str.length() != 0){
            count++;
            for (int i = 0; i < str.length(); i++) {
                if(str.charAt(i) == ' '){
                    count++;
                }
            }
        }
        return count;
    }
    public static String deleteLastString(String str){

        int lastIndex = str.lastIndexOf(" ");
        str = str.substring(0, lastIndex);
        return str;
    }

    public static String reverseString(String str){
        StringBuffer stringBuffer = new StringBuffer(str);
        return stringBuffer.reverse().toString();
    }

    public static String deleteLine(String str,String delete){
        return str.replace(str,"");
    }

    public static String editString(String str){
        return null;
    }
}
