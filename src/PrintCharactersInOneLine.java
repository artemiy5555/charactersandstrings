public class PrintCharactersInOneLine {

    public static void main(String[] args) {
        System.out.println(englishAlphabetBig());
        System.out.println(englishAlphabet());
        System.out.println(rusAlphabetBig());
        System.out.println(rusAlphabet());
        System.out.println(numAlphabet());
        ansii();
    }

    public static String englishAlphabetBig(){
        String eng ="";
        for(char i = 'A';i<='Z';i++) {
            eng+=i;
        }
        return eng;
    }
    public static String englishAlphabet(){
        String eng ="";
        for(char i = 'a';i<='z';i++) {
            eng+=i;
        }
        return eng;
    }
    public static String rusAlphabetBig(){
        String eng ="";
        for(char i = 'А';i<='Я';i++) {
            eng+=i;
        }
        return eng;
    }
    public static String rusAlphabet(){
        String eng ="";
        for(char i = 'а';i<='я';i++) {
            eng+=i;
        }
        return eng;
    }
    public static String numAlphabet(){
        String eng ="";
        for(char i = '0';i<='9';i++) {
            eng+=i;
        }
        return eng;
    }
    public static void ansii(){
        String eng ="";
        short i;
        for (i=32;i<=127;i++) {
            System.out.println(" %3d-%c"+i+i);
            if (i%10 == 0) System.out.println("\n");
        }
    }
}
