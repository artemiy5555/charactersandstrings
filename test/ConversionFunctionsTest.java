import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConversionFunctionsTest {

    @Test
    void test_stringToNumber(){

        String text = "6";

        assertEquals(6,ConversionFunctions.stringToNumber(text));
    }
    @Test
    void test_stringToDouble(){

        String text = "6.0";

        assertEquals(6.0,ConversionFunctions.stringToDouble(text));
    }

    @Test
    void test_numbersToStringInt(){

        int num = 6;

        assertEquals("6",ConversionFunctions.numbersToString(num));
    }

    @Test
    void test_numbersToStringDouble(){

        double num = 6;

        assertEquals("6.0",ConversionFunctions.numbersToString(num));
    }
}
