import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrintCharactersInOneLineTest {

    @Test
    void test_englishAlphabetBig(){
        assertEquals("ABCDEFGHIJKLMNOPQRSTUVWXYZ",PrintCharactersInOneLine.englishAlphabetBig());
    }

    @Test
    void test_englishAlphabet(){
        assertEquals("abcdefghijklmnopqrstuvwxyz",PrintCharactersInOneLine.englishAlphabet());
    }

    @Test
    void test_rusAlphabetBig(){
        assertEquals("АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",PrintCharactersInOneLine.rusAlphabetBig());
    }

    @Test
    void test_rusAlphabet(){
        assertEquals("абвгдежзийклмнопрстуфхцчшщъыьэюя",PrintCharactersInOneLine.rusAlphabet());
    }
}
