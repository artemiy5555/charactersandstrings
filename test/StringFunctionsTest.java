import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class StringFunctionsTest {
    @Test
    void test_shortestWordLength(){

        String text = "Просто пример предложения";

        assertEquals(6,StringFunctions.shortestWordLength(text));
    }

    @Test
    void test_editString(){

        String text = "Просто пример предложения";

        assertEquals(null,StringFunctions.editString(text));
    }

    @Test
    void test_addSpaces(){

        String text = "Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан.";

        assertEquals("Привет? Че!!! В рот вошло??? Ауф, мы волки... Это, фиаско; братан.",StringFunctions.addSpaces(text));
    }

    @Test
    void test_OneEks(){

        String text = "Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан.";

        assertEquals("ПЧ!Впше?Аумывл Э,фиско;бртан.",StringFunctions.OneEks(text));
    }

    @Test
    void test_countWords(){

        String text = "Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан.";

        assertEquals(5,StringFunctions.countWords(text));
    }

    @Test
    void test_revers(){

        String text = "Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан.";

        assertEquals(".натарб;оксаиф,отЭ ...иклов ым,фуА???тешап есВ !!!еЧ?тевирП",StringFunctions.reverseString(text));
    }
    @Test
    void test_deleteLine(){

        String text = "Привет?Че!!! Все пашет???Ауф,мы волки... Это,фиаско;братан.";

        assertEquals("Привет?Че!!! Все пашет???Ауф,мы волки...",StringFunctions.deleteLastString(text));
    }
}
